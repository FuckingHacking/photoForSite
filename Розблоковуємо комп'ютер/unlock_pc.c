#include <SPI.h>
#include <MFRC522.h>
#include <HID.h>
#include "Keyboard.h"
 
#define SS_PIN 10
#define RST_PIN 9
#define ZUMMER 4
 
MFRC522 mfrc522(SS_PIN, RST_PIN);
 
int* aux;
int cnt = 0;
byte UID[4] = {0xB6, 0xA9, 0xC6, 0x93};
int counter = 0; 
byte b = 9;// button push counter
char* PASSWD = "microsoft2012";
 
void setup() 
{
  pinMode(ZUMMER, OUTPUT);
  SPI.begin();
  mfrc522.PCD_Init();
  Keyboard.begin();
}
 
void loop() 
{
  verify();
}
 
void verify()
{
///////////////// ЗАПИТ НА ЗЧИТУВАННЯ ///////////////
  if ( ! mfrc522.PICC_IsNewCardPresent())          //
  {                                                //
    return;                                        //
  }                                                //
                                                   //
  if ( ! mfrc522.PICC_ReadCardSerial())            //
  {                                                //
    return;                                        //
  }                                                //
/////////////////////////////////////////////////////
 
  // Записуєм в масив UID зчитаної картки
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    aux[i]= mfrc522.uid.uidByte[i];
  } 
  
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    if(aux[i] == UID[i])
      cnt++;
  }
 
  if(cnt == 4)
  {
      tone(ZUMMER, 1200, 200);
      delay(500);
      Keyboard.write (KEY_ESC);
      delay(500); 
      Keyboard.press (KEY_LEFT_SHIFT);
      Keyboard.press (KEY_LEFT_ALT);
      Keyboard.releaseAll ();
      delay(250);
      Keyboard.press (KEY_LEFT_SHIFT);
      Keyboard.press (KEY_LEFT_ALT);
      Keyboard.releaseAll ();
      Keyboard.println(PASSWD); 
      delay(250);    
      Keyboard.write (KEY_RETURN);
  }
  else
  {
    tone(ZUMMER, 1700, 200);
    delay(200);
    tone(ZUMMER, 1700, 200);
    delay(200);
    tone(ZUMMER, 1700, 200);
    delay(200);
  }
  cnt=0;
}
